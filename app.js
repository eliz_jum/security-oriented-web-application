// https://github.com/naugtur/node-example-websec

const express = require('express')
const cookieParser = require('cookie-parser')
const crypto = require('crypto')
const bodyParser = require('body-parser')
const uuidV4 = require('uuid/v4')
var csrf = require('csurf')
const db = require('./db') // fake database

const app = express()

const sessionStorage = {}

var userLoggedIn;

// public static site
app.use('/public', express.static('public'))

// login handler
app.get('/public/login', (req, res) => {
  const uuid = uuidV4()
  const hash = crypto.createHash('sha256').update(req.query.password).digest('base64')
  userLoggedIn = req.query.login;
  if (req.query.login === 'root' && hash === 'jZae727K08KaOmKSgOaGzww/XVqGr/PKEgIMkjrcbJI=') {
    //haslo: 123456
    res.cookie('sid', uuid, { maxAge: 900000, httpOnly: true /*, secure: true */})
    sessionStorage[uuid] = {
      user: 'root'
    }
    setTimeout(() => {
      delete sessionStorage[uuid]
    }, 600000)
    res.redirect('/private')
  } else {
    res.status(403).redirect('/public')
  }
})

var csrfProtection = csrf({ cookie: false, sessionKEey:'session' })

// I know we're implementing everything to learn how, but cookie parsing... come on.
app.use('/private', cookieParser())

// Our session middleware
app.use('/private', (req, res, next) => {
  console.log('cookies', req.cookies)
  if (req.cookies.sid && sessionStorage[req.cookies.sid]) {
    req.session = sessionStorage[req.cookies.sid]
    next()
  } else {
    res.status(401).redirect('/public')
  }
})

app.post('/private/add', bodyParser.urlencoded(), csrfProtection, (req, res) => {
  db.get('posts')
    .push({ id: uuidV4(), user: userLoggedIn, title: req.body.title})
    .write()
  res.redirect('/private/form.html')
})
app.get('/private/posts', csrfProtection, (req, res) => {
  res.json({ csrfToken: req.csrfToken(), posts: db.get('posts')
    .value()})
})

app.post('/public/createAccount', bodyParser.urlencoded(), csrfProtection, (req, res) => {
  //var hashedPassword = crypto.createHash('sha256').update(req.body.password).digest('base64')
  users.get('users')
    //.push({ id: uuidV4(), login: req.body.login, password: hashedPassword})
    .push({ id: uuidV4(), login: req.body.login, password: req.body.password})

    .write()
userLoggedIn = req.body.login
res.redirect('/private/form.html')
})

// The walled garden
app.use('/private', express.static('private'))

// Have to choose some port, right
app.listen(1337)
